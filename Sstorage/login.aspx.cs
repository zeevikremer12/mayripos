﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sstorage.BLL;

namespace Sstorage
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LtlUserName.Text = "";
            LtlPassword.Text = "";
            LtlLogin.Text = "";
        }

        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            if (TxtUserName.Text == "")
                LtlUserName.Text = "נא הזן שם משתמש";
            if (TxtPassword.Text == "")
                LtlPassword.Text = "נא הזן סיסמה";
            if(TxtUserName.Text != ""&&TxtPassword.Text != "")
            {
                User user = new User();
                user.Name = TxtUserName.Text;
                user.Password = TxtPassword.Text;
                int uid = 0;
                uid = user.isExist(user);
                if (uid == 1)
                    Response.Redirect("index.aspx");
                else
                    LtlLogin.Text = "שם וסיסמא שגוים";
            }
        }
    }
}