﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sstorage.DAL;

namespace Sstorage
{ 
    namespace BLL
    {
        public class User
        {
            public int Uid { get; set; }
            public string Name { get; set; }
            public string Password { get; set; }
            public int Rid { get; set; }


            public User()
            {
                this.Uid = 0;
                this.Name = "";
                this.Password = "";
                this.Rid = 0;

            }
            public User(int Uid, string Name, string Password, int Rid)
            {
                this.Uid = Uid;
                this.Name = Name;
                this.Password = Password;
                this.Rid = Rid;
            }
            public int isExist(User user)
            {
                int uid;
                uid = UserDate.isExist(user);

                return uid;
            }

            public User getUser(int uid) 
            {
                User user = UserDate.getUser(uid);
                return user;
            }

        }
    }
   
}