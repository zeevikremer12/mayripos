﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Sstorage.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>התחברות | Sstorage</title>
    <link rel="stylesheet" type="text/css" href="css/style1.css" />
    <script type="text/javascript" src="js/js1.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            UserName<asp:TextBox ID="TxtUserName" runat="server" />
            <br />
            <asp:Literal ID="LtlUserName" runat="server" />
            <br />
            Password<asp:TextBox ID="TxtPassword" runat="server" />
            <br />
            <asp:Literal ID="LtlPassword" runat="server" />
            <br />
            <asp:Button ID="BtnLogin" Text="התחברות" runat="server" OnClick="BtnLogin_Click" />
            <br /><br />
            <asp:Literal ID="LtlLogin" runat="server" />
        </div>
    </form>
</body>
</html>
