CREATE TABLE [dbo].[T_users] (
    [UserId]     INT           IDENTITY (1, 1) NOT NULL,
    [UserName]   NVARCHAR (50) NOT NULL,
    [UserPassword]   NVARCHAR (50) NOT NULL,
    [RatingId]   INT           NOT NULL,
    [AddingDate] SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC)
);

