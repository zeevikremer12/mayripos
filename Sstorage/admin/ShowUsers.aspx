﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/adminMaster.Master" AutoEventWireup="true" CodeBehind="ShowUsers.aspx.cs" Inherits="Sstorage.admin.ShowUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<title> רשימת משתמשים | Sstorage</title> 
<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<link rel="stylesheet" type="text/css" href="css/util.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="limiter">
  <div class="container-table100">
    <div class="wrap-table100">
       <a href="addUser.aspx" style="color:#00ff21;">הוספת משתמש חדש</a>
        <a href="ShowUsers.aspx" style="color:#00ff21;">הצג משתמשים</a>
      <div class="table100" >                   
        <table>
          <thead id="titels">                     
          </thead>  
              <asp:Repeater ID="RptTable" runat="server">                
                <ItemTemplate>
                    <tr>              
              <td class="column1" style="text-align:center;width:12.5%;"><%#Eval("UserName") %></td>
              <td class="column2" style="text-align:center;width:12.5%;"><%#Eval("UserPassword") %></td>
              <td class="column3" style="text-align:center;width:12.5%;"><%#Eval("RatingId") %></td>
              <td class="column4" style="text-align:center;width:12.5%;"><%#Eval("AddingDate") %></td>
              <td class="column5" ><center><a href="removUser.aspx?Uid=<%#Eval("UserId") %>" >מחיקה</a></center></td>
              <td class="column5" ><center><a href="addUser.aspx?Uid=<%#Eval("UserId") %>" >עריכה</a></center></td>
              <td class="column5" ><center><a href="blockUser.aspx?Uid=<%#Eval("UserId") %>" >חסימה</a></center></td>
            </tr>                                                          
                </ItemTemplate>
            </asp:Repeater>                               
        </table>
      </div>
    </div>
  </div>
</div>
    <style>
        .column7{
            text-align:center;vertical-align:middle;width:12.5%; color:#ff0000;cursor:pointer; 
        }
        
    </style>    
<script src="UsersTitels.ashx"></script>
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/select2/select2.min.js"></script>
<script src="js/main.js"></script>
<script src="js/main1.js"></script>
<script>
    
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-23581568-13');
</script>
</asp:Content>
