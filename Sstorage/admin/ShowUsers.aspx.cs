﻿using Sstorage.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sstorage
{namespace admin
    {
        public partial class ShowUsers : System.Web.UI.Page
        {
            protected void Page_Load(object sender, EventArgs e)
            {
                if (!IsPostBack)
                {
                    FillData();
                }
            }
            void FillData()
            {
                SqlObject sObject = new SqlObject();
                DataTable Dt = new DataTable();
                Dt = sObject.FillTable("Tusers");
                RptTable.DataSource = Dt;
                RptTable.DataBind();


            }
        }
    }    
}