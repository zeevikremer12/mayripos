﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/adminMaster.Master" AutoEventWireup="true" CodeBehind="addUser.aspx.cs" Inherits="Sstorage.admin.addUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<title> הוספת משתמש | Sstorage</title> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    <br /><br /><center><table style="width:1005px;" border="0" >                                                                   
            <tr>                
                <td style="width:5%;"></td>
                <td style="width:30%;">
                    UserName<asp:TextBox ID="TxtUserName" runat="server" />
                    <br /><asp:Literal ID="LtlTxtUserName" runat="server" />
                </td>               
                <td style="width:30%;">
                    Password<asp:TextBox ID="TxtPassword" runat="server" />
                    <br /><asp:Literal ID="LtlTxtPassword" runat="server" />
                </td>
                <td style="width:30%;">
                    <asp:DropDownList ID="DdlRatings" runat="server" OnTextChanged="DdlRatings_TextChanged" AutoPostBack="true">                        
                    </asp:DropDownList>
                    <br /><asp:Literal ID="LtlDdlRatings" runat="server" />
                </td>
                <td style="width:5%;"></td>
            </tr>
            <tr >
                <td colspan="5">
                    <table border="0" style="width:1005px;" >
                        <tr style="width:16%;">
                            <td >
                                <br /><br />
                                <asp:CheckBox ID="cbAddingUsers"  runat="server" Text="הוספת משתמשים" Visible="false"/>
                            </td>
                            <td style="width:16%;">
                                <br /><br />
                                <asp:CheckBox ID="cbAddingStorages"  runat="server" Text="הוספת מחסנים/לקוחות" Visible="false"/>
                            </td>
                            <td style="width:16%;">
                                <br /><br />
                                <asp:CheckBox ID="cbCreatingOrders"  runat="server" Text="יצירת הזמנות" Visible="false"/>
                            </td>
                            <td style="width:16%;">
                                <br /><br />
                                <asp:CheckBox ID="cbCollectingOrders"  runat="server" Text="ליקוט הזמנות" Visible="false"/>
                            </td>
                            <td style="width:36%;">
                                <br /><br />
                                <asp:CheckBox ID="cbAddingSuppliers"  runat="server" Text="הוספת ספקים" Visible="false"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:16%;">
                                <br /><br />
                                <asp:CheckBox ID="cbAddingCategories"  runat="server" Text="הוספת קטגוריות" Visible="false"/>
                            </td>
                            <td style="width:16%;">
                                <br /><br />
                                <asp:CheckBox ID="cbAddingProducts"  runat="server" Text="הוספת מוצרים" Visible="false"/>
                            </td>
                            <td style="width:16%;">
                                <br /><br />
                                <asp:CheckBox ID="cbAddingStoragesLocators"  runat="server" Text="הוספת איתורי מחסנים" Visible="false"/>
                            </td>
                            <td style="width:16%;">
                                <br /><br />
                                <asp:CheckBox ID="cbInventoryUpdate"  runat="server" Text="עידכון מלאי" Visible="false"/>
                            </td>
                            <td style="width:36%;">
                                <br /><br />
                                <asp:CheckBox ID="cbOpeningSurfaces"  runat="server" Text="פתיחת משטחים" Visible="false"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:16%;">
                                <br /><br />                    
                                <asp:CheckBox ID="cbSurfacesLocatorUpdate"  runat="server" Text="עידכון איתורי משטחים" Visible="false"/>
                            </td>
                            <td style="width:16%;">
                                <br /><br />
                                <asp:CheckBox ID="cbExecutionInventoryMovements"  runat="server" Text="ביצוע תנועות מלאי" Visible="false"/>
                            </td>
                            <td style="width:16%;">
                                <br /><br />
                                <asp:CheckBox ID="cbAbsorptionProducts"  runat="server" Text="קליטת מוצרים" Visible="false"/>
                            </td>
                            <td style="width:16%;">
                                <br /><br />                    
                            </td>
                            <td style="width:36%;">
                            <br /><br />                    
                            </td>
                        </tr>                                    
                    </table>
                </td>
            </tr>  
        <tr>
                            <td colspan="5" >
                                <br /><br />
                                <center>
                                    <asp:Button ID="BtnOk" OnClick="BtnOk_Click" runat="server" Text="אישור" />
                                    <br /><asp:Literal ID="LtlAddUser" runat="server" />
                                </center>
                            </td>
                        </tr>
        </table>   
        </center>                                           
        <br /><br />            
        </div>
</asp:Content>
