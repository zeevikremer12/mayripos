﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/adminMaster.Master" AutoEventWireup="true" CodeBehind="suppliers.aspx.cs" Inherits="Sstorage.admin.suppliers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<title> ספקים | Sstorage</title>
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<link rel="stylesheet" type="text/css" href="css/util.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="limiter">
  <div class="container-table100">
    <div class="wrap-table100">
       <a href="addSupplier.aspx" >הוספת ספק חדש</a>
      <div class="table100" >                   
        <table>
          <thead>
            <tr class="table100-head">
              <th class="column1" style="text-align:center;width:12.5%;">קוד ספק</th>
              <th class="column2" style="text-align:center;width:12.5%;">שם</th>
              <th class="column3" style="text-align:center;width:12.5%;">טלפון</th>
              <th class="column4" style="text-align:center;width:12.5%;">אימייל</th>
              <th class="column5" style="text-align:center;width:12.5%;">קטגוריה</th>              
              <th class="column5" style="text-align:center;width:12.5%;">מחיקה</th>
              <th class="column5" style="text-align:center;width:12.5%;">עריכה</th>              
            </tr>
          </thead>  
              <asp:Repeater ID="RptTable" runat="server">                
                <ItemTemplate>
                    <tr>
              <td class="column1" style="text-align:center;width:12.5%;"><%#Eval("SupplierId") %></td>
              <td class="column2" style="text-align:center;width:12.5%;"><%#Eval("SupplierName") %></td>
              <td class="column3" style="text-align:center;width:12.5%;"><%#Eval("SupplierPhone") %></td>
              <td class="column4" style="text-align:center;width:12.5%;"><%#Eval("SupplierEmail") %></td>
              <td class="column5" style="text-align:center;width:12.5%;"><%#Eval("CatId") %></td>
              <td class="column7" ><a href="removSupplier.aspx?Uid=<%#Eval("SupplierId") %>" >מחיקה</a></td>
              <td class="column7" ><a href="addSupplier.aspx?Uid=<%#Eval("SupplierId") %>" >עריכה</a></td>              
            </tr>                                                          
                </ItemTemplate>
            </asp:Repeater>                               
        </table>
      </div>
    </div>
  </div>
</div>
    <style>
        .column7{
            text-align:center;vertical-align:middle;width:12.5%; color:#ff0000;cursor:pointer; 
        }
        
    </style>  
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/select2/select2.min.js"></script>
<script src="js/main.js"></script>
<script src="js/main1.js"></script>
<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-23581568-13');
</script>
</asp:Content>
