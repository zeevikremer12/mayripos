﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sstorage.BLL;
using Sstorage.DAL;




namespace Sstorage
{
    namespace admin
    {
        public partial class addUser : System.Web.UI.Page
        {
            protected void Page_Load(object sender, EventArgs e)
            {
                string Uid = "";
                LtlTxtUserName.Text = "";
                LtlTxtPassword.Text = "";
                LtlDdlRatings.Text = "";
                LtlAddUser.Text = "";
                if (DdlRatings.SelectedValue != "0")
                    noVisible();
                else
                    visible();
                if (!IsPostBack)
                {
                    FillDropDownList();
                    string url = Request.QueryString.ToString(); //url הפרמטרים שיש ב                
                    if (url != "")
                        FillDate(Request["Uid"]);
                }
            }

            protected void BtnOk_Click(object sender, EventArgs e)
            {
                if (TxtUserName.Text == "")
                    LtlTxtUserName.Text = "נא הזן שם משתמש";
                if (TxtPassword.Text == "")
                    LtlTxtPassword.Text = "נא הזן סיסמה";
                if (DdlRatings.SelectedValue  == "-1")
                    LtlDdlRatings.Text = "נא לבחור דרגה";
                if (TxtUserName.Text != "" && TxtPassword.Text != "" && DdlRatings.SelectedValue != "-1")
                {

                    LtlAddUser.Text = "המשתמש נוסף בהצלחה";
                }
            }
            public void FillDropDownList()
            {
                SqlObject sql = new SqlObject();
                DataTable Dt = new DataTable();
                Dt = sql.ExecuteKeyValue("Description", "RatingId", "Tratings");                
                DdlRatings.DataSource = Dt;
                DdlRatings.DataTextField = "TextField";
                DdlRatings.DataValueField = "ValueField";                
                DdlRatings.DataBind();
                sql.Close();                           
                ListItem l = new ListItem("התאם אישית", "0", true);                
                DdlRatings.Items.Add(l);
                ListItem j = new ListItem("בחר דרגה", "-1", true);
                j.Selected = true;
                DdlRatings.Items.Add(j);
            }
            public void noVisible()
            {
                cbAddingUsers.Visible = false;
                cbAddingStorages.Visible = false;
                cbCreatingOrders.Visible = false;
                cbCollectingOrders.Visible = false;
                cbAddingSuppliers.Visible = false;
                cbAddingCategories.Visible = false;
                cbAddingProducts.Visible = false;
                cbAddingStoragesLocators.Visible = false;
                cbInventoryUpdate.Visible = false;
                cbOpeningSurfaces.Visible = false;
                cbSurfacesLocatorUpdate.Visible = false;
                cbExecutionInventoryMovements.Visible = false;
                cbAbsorptionProducts.Visible = false;

            }
            public void visible()
            {
                cbAddingUsers.Visible = true;
                cbAddingStorages.Visible = true;
                cbCreatingOrders.Visible = true;
                cbCollectingOrders.Visible = true;
                cbAddingSuppliers.Visible = true;
                cbAddingCategories.Visible = true;
                cbAddingProducts.Visible = true;
                cbAddingStoragesLocators.Visible = true;
                cbInventoryUpdate.Visible = true;
                cbOpeningSurfaces.Visible = true;
                cbSurfacesLocatorUpdate.Visible = true;
                cbExecutionInventoryMovements.Visible = true;
                cbAbsorptionProducts.Visible = true;
            }
            public void FillDate(string Uid)
            {
                User user = new User();
                int uid = Convert.ToInt32(Uid);
                user = user.getUser(uid);
                TxtUserName.Text = user.Name;
                TxtPassword.Text = user.Password;
            }

            protected void DdlRatings_TextChanged(object sender, EventArgs e)
            {
                if (DdlRatings.SelectedValue == "0")
                {
                    visible();
                }
            }
        }
    }   
}